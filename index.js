function curry(a) {
  return function(b) {
    return a+b;
  }
}

//let getCurry = curry(1);
var obj = {
  name: 'Hulio',
  say(who) {
    return `Heya ${who}, I\'m ${this.name}`;
  }
}
var myObj = {name: 'Nazar'};
console.log(obj.say('Man'));
console.log(obj.say.call(myObj, 'Dude'));

var module = {
  x: 42,
  getX: function() {
    return this.x;
  }
}

var unboundGetX = module.getX.bind(module);
console.log(unboundGetX())
